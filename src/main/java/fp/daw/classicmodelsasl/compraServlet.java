package fp.daw.classicmodelsasl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import javax.swing.JOptionPane;

@WebServlet("/compra")
public class compraServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	Connection con = null;
		PreparedStatement stm = null;
		PreparedStatement stm2 = null;
		ResultSet rs = null;
		if (session.getAttribute("customer") == null) {
			response.sendRedirect("catalogo.jsp");
		}else {
			Map<String, LineaDetalle> ticket;
			ticket = (Map<String, LineaDetalle>) session.getAttribute("cart");
			for (Entry<String, LineaDetalle> entry : ticket.entrySet()) {
			    System.out.println("clave=" + entry.getKey() + ", valor=" + entry.getValue().getProductName());
			    try {
				    Context context = new InitialContext();
					DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
					con = ds.getConnection();
					stm=con.prepareStatement("SELECT quantityInStock FROM products WHERE productCode = ?");
					stm.setString(1 , entry.getValue().getProductCode());
					rs = stm.executeQuery();
					rs.next();	
					int code = Integer.parseInt(rs.getString(1));
					
					code = code-entry.getValue().getAmount();
					
					stm2 = con.prepareStatement("UPDATE products SET quantityInStock = ? WHERE productCode = ?");
					stm2.setInt(1, code);
					stm2.setString(2 , entry.getValue().getProductCode());
					int rs2 = stm2.executeUpdate();
			        if(rs2 > 0){
			        
			        //session.invalidate();	
			        	/*request.removeAttribute("c");
						request.removeAttribute("n");
						request.removeAttribute("p");*/
			        //response.sendRedirect("postcompra.jsp");
			        String page = "";
			        try {

			        } catch (Exception e) {
			          page = "error.jsp";
			        } finally {
			          page = "postcompra.jsp";
			        }

			        RequestDispatcher dd=request.getRequestDispatcher(page);
			        dd.forward(request, response);
			        
			            
			        }else{
			        
			            JOptionPane.showMessageDialog(null, "No se ha podido realizar la actualización de los datos\n"
			                                          + "Inténtelo nuevamente.", "Error en la operación", 
			                                          JOptionPane.ERROR_MESSAGE);
			        
			        }
				} catch (NumberFormatException | SQLException | NamingException e) {
					e.printStackTrace();
				}
			}
			
		}
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
