package fp.daw.classicmodelsasl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;


import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class HashUtils {
	private static SecureRandom secureRandom = new SecureRandom();

	public static byte[] getRandomSalt(int length) {
		byte[] salt = new byte[length];
		secureRandom.nextBytes(salt);
		return salt;
	}

	public static byte[] getHash(String password, int lenght, byte[] salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 0xffff, lenght);
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		SecretKey key = factory.generateSecret(spec);
		return key.getEncoded();
	}

	public static String getBase64Hash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] salt = getRandomSalt(66);
		byte[] hash = getHash(password, 512, salt);
		String hashBase64 = Base64.getEncoder().encodeToString(hash);
		String saltBase64 = Base64.getEncoder().encodeToString(salt);
		return hashBase64.substring(0, 21) + saltBase64 + hashBase64.substring(21);
	}

	public static boolean validateBase64Hash(String password, String savedHash)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] salt = Base64.getDecoder().decode(savedHash.substring(21, 109));
		byte[] hash1 = Base64.getDecoder().decode(savedHash.substring(0, 21) + savedHash.substring(109));
		byte[] hash2 = getHash(password, 512, salt);
		return Arrays.compare(hash1, hash2) == 0;
	}

	public static String getBase64Digest(String message) throws NoSuchAlgorithmException {
		byte[] hash = MessageDigest.getInstance("SHA-256").digest(message.getBytes());
		return Base64.getUrlEncoder().encodeToString(Arrays.copyOf(hash, 33));
	}
	
	
	
	
	
	
	public static String getBaseHexadecimalHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] salt = getRandomSalt(66);
		byte[] hash = getHash(password, 512, salt);
		String hashBaseHexadecimal = toHex(hash);
		String saltBaseHexadecimal = toHex(salt);
		return hashBaseHexadecimal.substring(0, 21) + saltBaseHexadecimal + hashBaseHexadecimal.substring(21);
	}

	public static boolean validateBaseHexadecimalHash(String password, String savedHash)throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		
		byte[] salt = toByte(savedHash.substring(21, 109));
		byte[] hash1 = toByte(savedHash.substring(0, 21) + savedHash.substring(109));
		byte[] hash2 = getHash(password, 512, salt);
		return Arrays.compare(hash1, hash2) == 0;
		
		
	}
	

		public static final String toHex(byte[] data) {
	        final StringBuffer sb = new StringBuffer(data.length * 2);
	        for (int i = 0; i < data.length; i++) {
	            sb.append(Character.forDigit((data[i] >>> 4) & 0x0F, 16));
	            sb.append(Character.forDigit(data[i] & 0x0F, 16));
	        }
	        return sb.toString();
	    }
	    
	    
	    
		private static byte[] toByte(String hexString) {
			byte[] buffer = new byte[hexString.length() / 2];
	          int i = 2;
	          while (i < hexString.length())
	          {
	              buffer[i / 2] = (byte)Integer.parseInt(hexString.substring(i, i + 2),16);
	              i += 2;
	          }
	          return buffer;
	    }
	    

	    
	    
	    

	    
	    
	    
	    
	    
	    
	
	
	    
	    
	    
	
}
