package fp.daw.classicmodelsasl;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
//
public class MailService {
//
	public static void sendMessage(String smtpServer, String port, String account, String password, String to,
		String subject, String body, String type) throws MessagingException {
			Properties properties = new Properties();
			properties.setProperty("mail.smtp.host", smtpServer);
			properties.setProperty("mail.smtp.starttls.enable", "true");
			properties.setProperty("mail.smtp.port", port);
			if (account != null && password != null)
				properties.setProperty("mail.smtp.auth", "true");
			Session mailSession = Session.getInstance(properties);
			mailSession.setDebug(true);
			Message message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(account));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setContent(body, type);
			message.setSentDate(new Date());
			Transport.send(message, account, password);
		}

	static void sendConfirmationMessage(String email, String confirmationCode, String name) throws MessagingException {
		StringBuilder body = new StringBuilder();
		
		//cuidado con los puertos
		body.append("<form action =\"http://localhost:8081/ClassicModelsAsl/confirm\"");
		body.append(" method=\"POST\">");
		body.append(" <input type=\"hidden\" name =\"email\" value =\"");
		body.append(email);
		body.append("\"/>");
		
		body.append(" <input type=\"hidden\" name =\"cc\" value =\"");
		body.append(confirmationCode);
		body.append("\"/>");
		
		body.append("<h1>Hola, "+name+"!</h1>");
		body.append("<p>Necesitamos confirmar tu direccion de correo electrónico para completar tu registro de Classic Models</p>");
		body.append("<p> <input type=\"submit\" name =\"cc\" value =\"Confirmar\" /> </p>");
		
		body.append("</form>");
		sendMessage( 
				"smtp.gmail.com",
				"587",
				"classicmodelsfp@gmail.com",
				"FP2021cifp",
				email,
				"Confirma tu dirección de correo electrónico",
				body.toString(), 
				"text/html" );
		}

}
//