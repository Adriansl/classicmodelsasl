package fp.daw.classicmodelsasl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
//
@WebServlet("/reenvio")
public class reenvioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
//
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection con = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		String code = null;
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		if (name == null || surname == null || email == null)
			response.sendRedirect("catalogo.jsp");
		else {
					try {
						Context context = new InitialContext();
						DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
						con = ds.getConnection();
						stm=con.prepareStatement("SELECT confirmationCode FROM signups WHERE customerEmail = ?");
						stm.setString(1 , email);
						rs = stm.executeQuery();
						rs.next();
						code = rs.getString(1);
					} catch (NamingException | SQLException e1) {
						e1.printStackTrace();
					}
				try {
					MailService.sendConfirmationMessage(email, code, name);
					response.sendRedirect("postregistro.jsp?email=" + email + "&nombre=" + name + "&apellidos=" + surname);
				} catch (MessagingException e) {
					e.printStackTrace();
				}
		}
	}
//
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
//	
	
}
