package fp.daw.classicmodelsasl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.swing.JOptionPane;

@WebServlet("/confirm")
public class confirmacionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Connection con = null;
		PreparedStatement stm = null;
		
		
		String email = request.getParameter("email");
		String cc = request.getParameter("cc");
		

		try {
			/*		Comprobar por que no conecta
			Connection conexion = DriverManager.getConnection ("jdbc:mysql://localhost:3306/classicmodels","classicmodels", "practicas");
			String consulta = "UPDATE signups SET confirmationCode=? WHERE customerEmail=? AND confirmationCode=?";
			PreparedStatement sentencia= conexion.prepareStatement(consulta);
			sentencia.setNull(1, 1);
			sentencia.setString(2, email);
			sentencia.setString(3, cc);

			int rs = sentencia.executeUpdate();		
			*/
			Context context = new InitialContext();
			DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
			con = ds.getConnection();
			stm = con.prepareStatement("UPDATE signups SET confirmationCode = ? WHERE customerEmail = ? AND confirmationCode = ?");
			stm.setNull(1, 1);
			stm.setString(2, email);
			stm.setString(3, cc);
			
			int rs = stm.executeUpdate();
			        if(rs > 0){
			        
			            
			            response.sendRedirect("catalogo.jsp");
			            
			        }else{
			        
			            JOptionPane.showMessageDialog(null, "No se ha podido realizar la actualización de los datos\n"
			                                          + "Inténtelo nuevamente.", "Error en la operación", 
			                                          JOptionPane.ERROR_MESSAGE);
			        
			        }
			
		} catch (SQLException e) {
			  JOptionPane.showMessageDialog(null, "No se ha podido realizar la actualización de los datos\n"
                      + "Inténtelo nuevamente.\n"
                      + "Error: "+e, "Error en la operación", 
                      JOptionPane.ERROR_MESSAGE);
		} catch (NamingException e) {
			  JOptionPane.showMessageDialog(null, "No se ha podido realizar la actualización de los datos\n"
                      + "Inténtelo nuevamente.\n"
                      + "Error: "+e, "Error en la operación", 
                      JOptionPane.ERROR_MESSAGE);
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
