<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%
if (session.getAttribute("user") != null)
	response.sendRedirect("catalogo.jsp");
	else {
		String firstName = request.getParameter("nombre");
		String lastName = request.getParameter("apellidos");
		String email = request.getParameter("email");
		%>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="ISO-8859-1">
				<title>Registro</title>
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
			</head>
			<body class="jumbotron card">
				<div id="white">
					<h1>
						Bienvenido
						<%=firstName%> <%=lastName%></h1>
					<hr />
					<p>
						Sólo queda un paso más para completar tu registro y tener acceso a
						todos nuestros Servicios, sigue las instrucciones que te hemos enviado
						a
						<%=email%></p>
					<p>
						Pulsa el botón de reenviar para que te volvamos a enviar el
						mensaje de confirmación:
					</p>
				</div>
				<div>
					<form action="reenvio" method="post">
						<input type="hidden" name="email" value="<%=email%>" />
						<input type="hidden" name="name" value="<%=firstName%>" />
						<input type="hidden" name="surname" value="<%=lastName%>" />
						<p>
							<input type="submit" value="Reenviar" class="btn btn-primary"/>
						</p>
					</form>
					
					<a href="login.jsp" class="btn btn-primary">Iniciar sesión</a>
					
					<a href="catalogo.jsp" class="btn btn-primary">Página de inicio</a>
					
				</div>
			</body>
		</html>
		<style type="text/css">
			body{
				margin-left: 50px;
				margin-right: 50px;
			}
			th{
				vertical-align:top;
			}
			#white{
				background-color: white;
			}
			div{
				padding: 20px;
				margin: 20px;
			}
		</style>
		
		<%
	}

%>