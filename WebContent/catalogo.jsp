<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<sql:query var="modelos" dataSource="jdbc/classicmodels">
 	select productCode, productName, productScale, productDescription, MSRP from products;
</sql:query>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Catálogo de Productos</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</head>
<body>
	<header class="jumbotron">
		<h1>Catálogo de Modelos a Escala</h1>
		<hr/>
		<c:choose>
			<c:when test="${not empty sessionScope.customer}">
				<p>${sessionScope.customer}</p>
				<p><a class="btn btn-primary" href="cart.jsp">Carrito</a></p>
				<p><a class="btn btn-primary" href="logout.jsp">Cerrar Sesion</a></p>
			</c:when>
			<c:otherwise>
				<p>Session no iniciada</p>
				<p><a class="btn btn-primary" href="login.jsp">Iniciar Sesion</a> <a class="btn btn-primary" href="registro.jsp">Registrarse</a></p>
			</c:otherwise>
		</c:choose>
	</header>
	<div class="row">
		<table class="jumbotron table table-striped table-bordered table-sm" cellspacing="0" width="100%" id="myTable">
			<thead>
			    <tr>
				     <th>Nombre</th>
				     <th>Escala</th>
				     <th>Descripcion</th>
				     <th>Precio</th>
				     <c:if test="${not empty sessionScope.customer}">
						<th>Añadir</th>	
					 </c:if>
			    </tr>
			</thead>
			<tbody>
				<c:forEach var="modelo" items="${modelos.rows}">
					<tr>
						<td class="text-left" id="white">${modelo.productName}</td>
						<td class="text-left" id="white">${modelo.productScale}</td>
						<td class="text-left" id="white">${modelo.productDescription}"</td>
						<td class="text-left" id="white">${modelo.MSRP}€</td>
						<c:if test="${not empty sessionScope.customer}">
							<td colspan="2"><a class="btn btn-primary" href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}"><img style='width: 1em; heigth: 1em;' src="img/cart-plus2.svg"/></a></td>
						</c:if>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
<script type="text/javascript">
	$(document).ready(function(){
	    $('#myTable').dataTable();
	});
</script>
</body>

<style type="text/css">
	body{
		margin-left: 50px;
		margin-right: 50px;
	}
	#white{
		background-color: white;
		padding-right: 5px;
		padding-left: 5px;
	}
	a{
		margin-right: 10px;
	}
</style>
</html>

