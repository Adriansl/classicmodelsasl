<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<c:if test="${empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Carrito</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
	<div class="jumbotron">
		<h1>Carrito de la compra</h1>
		<p><a class="btn btn-primary" href="catalogo.jsp">Catálogo</a></p>
		<c:set var="total" scope ="session" value ="${0}"></c:set>
		<c:choose>
			<c:when test="${empty sessionScope.cart}">
				<p>No se han añadido productos al carrito</p>
			</c:when>
			<c:otherwise>
				<table class="jumbotron table table-striped table-bordered table-sm">
					<tr>
						<th>Producto</th>
						<th>Unidades</th>
						<th>Precio</th>
						<th>Importe</th>
					</tr>
					<c:forEach var="linea" items="${sessionScope.cart}">
						<tr>
							<td id="white">${linea.value.productName}</td>
							<td id="white">${linea.value.amount}</td>
							<td id="white">${linea.value.price}</td>
							<td id="white">${linea.value.amount * linea.value.price}</td>
							<c:set var="total" scope ="session" value ="${total+(linea.value.amount*linea.value.price)}"></c:set>
						</tr>
					</c:forEach>
				</table>
				<table class="jumbotron table table-striped table-bordered table-sm">
					<tr>
						<th colspan="2">Total:</th>
						<td id="white">${total}€</td>
						<td><a href="compra" class="btn btn-primary">Comprar</a></td>
					</tr>
				</table>
			</c:otherwise>		
		</c:choose>
	</div>
</body>
</html>
<style type="text/css">
	body{
		margin-left: 50px;
		margin-right: 50px;
	}
	#white{
		background-color: white;
		padding-right: 5px;
		padding-left: 5px;
	}
	a{
		margin-right: 10px;
	}
</style>