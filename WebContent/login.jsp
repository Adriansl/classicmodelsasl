<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<c:if test="${not empty sessionScout.customer}">
	<c:redirect url="catalogo.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inicio de Sesi�n</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
		<form action="login" method="post" class="jumbotron card">
			<h1>Inicio de sesi�n:</h1>
			<hr/>
			<p>
				<label for="email">eMail:</label>
			</p>
			<p>
				<input type="email" name="email" required="required" />
			</p>
			<p>
				<label for="password">Contrase�a:</label>
			</p>
			<p>
				<input type="password" name="password" required="required" />
			</p>
			<p>
				<input type="submit" value="Login" class="btn btn-primary"/>
				<a class="btn btn-primary" href="registro.jsp">Registrarse</a>
				<a href="catalogo.jsp" class="btn btn-primary">P�gina de inicio</a>
			</p>
			
		</form>
		
		<c:choose>
			<c:when test="${param.status == 1}">
				<p>Las credenciales del usuario no son correctas</p>
			</c:when>
			<c:when test="${param.status == 2}">
				<p>Error del sistema, pongase en contacto con el administrador</p>
			</c:when>
			<c:when test="${param.status == 3}">
				<%
					String email = (String) session.getAttribute("email");
					String name = (String) session.getAttribute("nombre");
					String surname = (String) session.getAttribute("apellidos");
					response.sendRedirect("postregistro.jsp?email=" + email + "&nombre=" + name + "&apellidos=" + surname);
				%>
			</c:when>
		</c:choose>
</body>
</html>
<style type="text/css">
	body{
		margin: 200px;
	}
	form{
		padding-left: 20px;
	}
</style>

