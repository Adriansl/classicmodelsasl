<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<c:if test="${not empty sessionScout.customer}">
	<c:redirect url="catalogo.jsp"></c:redirect>
</c:if>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Registro de Usuarios</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script>
		function check(confirm) {
			if (confirm.value != document.getElementById('password').value) {
				confirm.setCustomValidity('Las contraseñas no coinciden');
			} else
				confirm.setCustomValidity('');
		}
	</script>
</head>
<body>
	<form action="signup" method="post" onsubmit="check()" class="jumbotron card">
		<h1>Registro de usuario</h1>
		<hr />
		
		
		<p>
			<label for="nombre">Nombre</label>
		</p>
		
		<p>
			<input type="text" name="nombre" required="required" />
		</p>
		
		
		<p>
			<label for="apellidos">Apellidos</label>
		</p>
		
		<p>
			<input type="text" name="apellidos" required="required" />
		</p>
		
		
		
		<p>
			<label for="email">eMail</label>
		</p>
		
		<p>
			<input type="email" name="email" required="required" />
		</p>
		<p>
			<label for="password">Contraseña</label>
		</p>
		<p>
			<input type="password" name="password" id="password"
				required="required" />
		</p>
		<p>
			<label for="password">Confirmar contraseña</label>
		</p>
		<p>
			<input type="password" id="confirm" required="required"
				oninput="check(this)" />
		</p>
		<p>
			<input type="submit" value="Registrar Usuario" class="btn btn-primary"/>
			<a class="btn btn-primary" href="login.jsp">Iniciar sesion</a>
			<a href="catalogo.jsp" class="btn btn-primary">Página de inicio</a>
		</p>
	</form>
		<c:choose>
			<c:when test="${param.status == 1}">
				<p>El email ya está registrado</p>
			</c:when>
			<c:when test="${param.status == 2}">
				<p>Error del sistema, pongase en contacto con el administrador</p>
			</c:when>
		</c:choose>
</body>
</html>
<style type="text/css">
	body{
		margin: 200px;
	}
	form{
		padding-left: 20px;
	}
</style>